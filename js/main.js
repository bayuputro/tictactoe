$(function(){
    UI.messageBox = $("#message");
    UI.targetDraw = $("#fields");
    UI.body = $("body");

    var turn = 0;

    //building game
    Game.init();

    //building layout
    UI.init();


    //first turn from enemy
    setTimeout(function(){
        UI.action_draw([1,1],Game.cpuMark); 
        main()
        UI.showMessage('Your turn.');
        },1000);


    function main(){



        $("#fields li").each(function(){
            var t = $(this);

            t.click(function(){
                if(is_player_turn()){


                    if(!$(this).html().length){
                        //save data state
                        var col = $(this).attr('col');
                        var row = $(this).attr('row');

                        Game.board[col][row] = Game.playerMark; 

                        //render the mark to screen
                        $(this).html(Game.playerMark); 

                        //check if any line formed by player
                        if(Game.is_line_formed(Game.playerMark)){
                            UI.showModal('YOU WIN!');
                        }

                        swich_turn();
                        UI.showMessage('CPU turn.');
                        if(Game.is_gameover()){
                            console.log("GAME OVER");
                        }else{
                            //enemy movement
                            setTimeout(function(){
                                AI.think();
                                swich_turn();
                                UI.showMessage('Your turn.');
                                },1000);
                        }
                    }


                }else{
                    console.log('enemy turn');
                }
            })
        })
    }


    function swich_turn(){
        turn++;
    }

    function is_player_turn(){
        if(turn % 2 ==0) return true;
        return false;
    }


    $("#btRestart").click(function(){
        window.location.reload();
    })
})