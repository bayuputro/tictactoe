/**
* Game is used for controlling the whole game
* 
* @author Bayu Putra <bayuputra_mail@yahoo.com>
* @type Object
*/

var Game = {
    
    boardSize: 3,
    
    board: [],
    
    init: function(){
        //creating board data based on board size
        for(var i=0; i < this.boardSize; i++){
           this.board.push([]); 
        }
        
       
    },

    playerMark: "<span style='color:green'>O</span>",
    cpuMark:  "<span style='color:red'>X</span>",

    //to down is filled
    is_row_filled: function (row){
        for(var i=0;i<this.boardSize;i++){
            if(Game.board[i][row]==null){
                return true;
            }
        }

        return false;
    },

    //to left is filled
    is_column_filled: function(col){
       for(var i=0;i<this.boardSize;i++){
            if(Game.board[col][i]==null){
                return true;
            }
        }

        return false;
    },

    is_2_true: function(lined){
        if(array_count(lined,true)==2){
            return true;
        }
    },

    is_all_true: function(data){
        for(var index in data){
            if(data[index]==false) return false;
        }

        return true;
    },

    //all filled
    is_all_filled: function(){
        for(var col=0; col < this.boardSize; col++){

            for(var row=0; row < this.boardSize; row++){

                if(Game.board[col][row] == undefined){
                    console.log("col,row",col,row,Game.board[col][row]);
                    return false;
                }
            }  
        }

        return true;

    },

    get_false_pos:function (lined){
        for(var index in lined){
            if(lined[index] == false){
                return index;
            }
        }

        return -1;
    },

    is_line_formed: function(mark){

        //check horizonal lines
        if(this.horizontal_lined(mark,Game.board)){
            console.log('Win Horizontal');
            return true;
        }

        if(this.vertical_lined(mark,Game.board)){
            console.log('Win Vertical');
            return true;
        }

        if(this.diagonal_lined(mark,Game.board)){
            console.log('Win Diagonal');
            return true;
        }


    },

    horizontal_lined: function(mark,data){

        for(var i=0;i < this.boardSize;i++){

            var row = i;
            var lined = [];
            for(var col in data){
                if(data[col][row]==mark){
                    lined[col] = true;
                }else{
                    lined[col] = false;
                }
            }

            if(this.is_all_true(lined)){
                return true;   
            }
        }

        return false;

    },

    vertical_lined: function(mark,data){

        for(var i=0;i < this.boardSize;i++){

            var col = i;
            var lined = [];
            for(var row in data){
                if(data[col][row]==mark){
                    lined[row] = true;
                }else{
                    lined[row] = false;
                }
            }

            if(this.is_all_true(lined)){
                return true;   
            }
        }

        return false;

    },

    diagonal_lined: function(mark,data){
        if(data[0][0]==mark && data[1][1]==mark && data[2][2]==mark){
            return true;
        }

        if(data[0][2]==mark && data[1][1]==mark && data[2][0]==mark){
            return true;
        }
    },
    
    is_gameover: function(){
        return Game.is_line_formed(Game.playerMark) ||  Game.is_line_formed(Game.cpuMark);
    }


}