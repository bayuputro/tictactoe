/**
* AI is used for CPU brain, determine how the enemy movement
* 
* @author Bayu Putra <bayuputra_mail@yahoo.com>
* @type Object
*/

var AI = {
    think: function(){

        var AIAction = [];
        
        //look all horizonal and vertical line
        for(var i=0; i<Game.boardSize; i++){
            AIAction.push(this.lookOnHorizontalLine(i,Game.playerMark));
            AIAction.push(this.lookOnVerticalLine(i,Game.playerMark));
        }

        //console.log(AIAction);

        //If AI not find any treating line, draw random
        if(AIAction.indexOf(true)==-1){
            console.log('Random draw!');
            UI.action_draw_random(Game.cpuMark,Game.board);
        }

        if(Game.is_line_formed(Game.cpuMark)){
            UI.showModal('YOU LOST!');
        }

        return false;
    },

    lookOnHorizontalLine: function(row){         
        //skip if the whole column is filled
        if(Game.is_row_filled(row)){
            var candidate = [];
            var lined = [];
            for(var i=0; i<Game.boardSize;i++){
                console.log('i = ' + i)
                if(Game.board[i][row]==Game.playerMark){
                    lined[i] = true;
                }else{
                    lined[i] = false;
                }
            }

            console.log(lined);

            if(Game.is_2_true(lined)){
                candidate.push([Game.get_false_pos(lined),row]);  
            }

            //player try to make line
            if(candidate.length > 0){
                console.log("Lined candidate founded, CPU will block it!");
                var pos = candidate[0];
                UI.action_draw([pos[0],pos[1]],Game.cpuMark,Game.board);

                return true;

            }else{
                return false;
            }
        }else{               
            return false;
        }
    },

    lookOnVerticalLine: function(col){
        if(Game.is_column_filled(col)){
            var candidate = [];
            var lined = [];
            for(var i=0;i<Game.boardSize;i++){
                if(Game.board[col][i]==Game.playerMark){
                    lined[i] = true;
                }else{
                    lined[i] = false;
                }
            }

            if(Game.is_2_true(lined)){
                candidate.push([col,Game.get_false_pos(lined)]);  
            }

            //player try to make line
            if(candidate.length > 0){
                console.log("Lined candidate founded, CPU will block it!");

                var pos = candidate[0];
                UI.action_draw([pos[0],pos[1]],Game.cpuMark,Game.board);

                return true;

            }else{
                return false;
            }
        }else{               
            return false;
        }

    }
}