/**
* UI used for managing user interface
* 
* @author Bayu Putra <bayuputra_mail@yahoo.com>
* @type Object
*/

var UI = {
    messageBox: null,
    targetDraw: null,
    body: null,
    modal: null,

    init: function(){
        for(i=0;i<Game.boardSize;i++){
            for(j=0;j<Game.boardSize;j++){
                li = $('<li id="'+i + '-' + j +'" col="'+i+'" row="'+j+'"></li>');
                this.targetDraw.append(li);           
            }
        }
    },

    showModal: function(message){
        this.modal = $('<div id="boardblocker" />');
        this.modal.append('<d<div class="message">'+message+'</div>');
               
        this.body.append(this.modal);
    },

    hideModal: function(){
        body.remove(this.modal);
    },

    showMessage: function(message){
        this.messageBox.html(message);
    },

    action_draw_random: function (mark){

        var success = false;

        while(!success){
            var randX = get_random_pos(); 
            var randY = get_random_pos(); 
            console.log("random: ",randX,randY);
            if(Game.board[randX][randY] == null){
                console.log('draw ' + mark + ' on: ',randX,randY);
                success = true;  

            }
        }

        Game.board[randX][randY] = mark;

        $("#"+randX+"-"+randY).html(mark);
    },

    action_draw: function(position,mark){    
        Game.board[position[0]][position[1]] = mark;
        $("#"+position[0]+"-"+position[1]).html(mark);
    },

    render_mark: function(){

    }

}